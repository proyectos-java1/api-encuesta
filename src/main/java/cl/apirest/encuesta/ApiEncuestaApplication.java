package cl.apirest.encuesta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"cl.apirest.encuesta"})
public class ApiEncuestaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiEncuestaApplication.class, args);
	}

}
