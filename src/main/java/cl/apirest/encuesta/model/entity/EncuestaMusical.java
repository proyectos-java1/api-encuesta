package cl.apirest.encuesta.model.entity;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@Entity
@Table(name = "detalle_encuestas")
public class EncuestaMusical implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String emailUsuario;

    @Enumerated(value = EnumType.STRING)
    private MusicStyle musicStyle;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern="dd-MM-yyyy HH:mm:ss")
    private Date createAt;


    @PrePersist
    public void preSave() { createAt = new Date(); }

    public enum MusicStyle {
        Rock,
        Pop,
        Jazz,
        Clásica,
        Disco,
        Electronica
    }
}
