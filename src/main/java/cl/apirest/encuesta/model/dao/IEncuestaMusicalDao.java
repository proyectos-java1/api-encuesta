package cl.apirest.encuesta.model.dao;

import cl.apirest.encuesta.model.entity.EncuestaMusical;
import org.springframework.data.repository.CrudRepository;

public interface IEncuestaMusicalDao extends CrudRepository <EncuestaMusical, Long> {
}
