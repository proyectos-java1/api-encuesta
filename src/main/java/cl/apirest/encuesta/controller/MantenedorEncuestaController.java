package cl.apirest.encuesta.controller;

import cl.apirest.encuesta.config.SwaggerConfig;
import cl.apirest.encuesta.model.entity.EncuestaMusical;
import cl.apirest.encuesta.service.IEncuentaMusicalService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@CrossOrigin(origins = {"http://127.0.0.1:5173", "http://localhost:3000",
                        "http://localhost:4200", "http://127.0.0.1"})
@Api(value = "Controlador Encuestas")
@Import(SwaggerConfig.class)
@RestController
@RequestMapping("/api/v1")
public class MantenedorEncuestaController {

    Logger log = LoggerFactory.getLogger(MantenedorEncuestaController.class);

    @Autowired
    private IEncuentaMusicalService encuentaMusicalService;

    @ApiOperation(value = "Lista los datos guardados", response = MantenedorEncuestaController.class)
    @GetMapping("/getListaEncuesta")
    public List<EncuestaMusical> getListaMusical() {

        try {
            if (encuentaMusicalService.findAll().isEmpty()){
                log.error("No hay datos disponibles!!");
            }
            return encuentaMusicalService.findAll();
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error: No se encontraron datos!!", ex);
        }
    }

    @ApiOperation(value = "Guarda encuesta nueva de música", response = MantenedorEncuestaController.class)
    @PostMapping("/saveEncuesta")
    public EncuestaMusical addNewEncuesta(@RequestBody EncuestaMusical addEncuesta){

        try {
            if (addEncuesta == null) {
                log.error("Error: Datos de entrada vacíos");
            }
            return encuentaMusicalService.save(addEncuesta);

        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error: No se pudo grabar la encuesta!", ex);
        }
    }
}
