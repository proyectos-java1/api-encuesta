package cl.apirest.encuesta.service;

import cl.apirest.encuesta.model.entity.EncuestaMusical;
import java.util.List;

public interface IEncuentaMusicalService {

    List<EncuestaMusical> findAll();

    EncuestaMusical save(EncuestaMusical encuestaMusical);
}
