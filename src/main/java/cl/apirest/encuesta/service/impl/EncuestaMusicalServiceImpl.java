package cl.apirest.encuesta.service.impl;

import cl.apirest.encuesta.model.dao.IEncuestaMusicalDao;
import cl.apirest.encuesta.model.entity.EncuestaMusical;
import cl.apirest.encuesta.service.IEncuentaMusicalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EncuestaMusicalServiceImpl implements IEncuentaMusicalService {

    @Autowired
    private IEncuestaMusicalDao encuestaMusicalDao;

    @Override
    public List<EncuestaMusical> findAll() { return (List<EncuestaMusical>) encuestaMusicalDao.findAll(); }

    @Override
    public EncuestaMusical save(EncuestaMusical encuestaMusical) {
        return encuestaMusicalDao.save(encuestaMusical);
    }
}
